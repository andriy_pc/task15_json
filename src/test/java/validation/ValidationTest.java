package validation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidationTest {

    @Test
    void validate() {
        Validation v = new Validation();
        boolean r = v.validate("D:\\WorkSpace\\json\\src\\main\\resources\\schema.json",
                "D:\\WorkSpace\\json\\src\\main\\resources\\planes.json");
        assertTrue(r);
    }
}