import app.gson.UseGson;
import org.junit.jupiter.api.Test;
import units.Planes;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

class UseGsonTest {

    @Test
    void fromGson() {
        UseGson u = new UseGson();
        Planes p = u.fromGson();
        assertNotNull(p);
    }

    @Test
    void toGson() {
        UseGson u = new UseGson();
        Planes p = u.fromGson();
    }
}