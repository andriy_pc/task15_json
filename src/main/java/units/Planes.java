package units;

import java.util.ArrayList;

public class Planes {
    private ArrayList<Plane> Planes = new ArrayList<>();

    public ArrayList<Plane> getPlanes() {
        return Planes;
    }

    public void setPlanes(ArrayList<Plane> planes) {
        this.Planes = planes;
    }

    public void addToPlanes(Plane p) {
        Planes.add(p);
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for(Plane p : Planes) {
            result.append(p.toString());
            result.append("\n");
        }
        return result.toString();
    }
}
