package app;

import app.gson.UseGson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import units.Planes;
import validation.Validation;

public class Model {
    private static Logger log = LogManager.getLogger();
    private Validation validation;
    private UseGson gson = new UseGson();

    public Model() {
        validation = new Validation();
        gson = new UseGson();
    }

    public void validate() {
        boolean result = validation.validate("files\\schema.json",
                "files\\planes.json");
        log.info("schema is valid: " + result);
    }

    public void readGson() {
        Planes result = gson.fromGson();
        System.out.println(result);
    }

    public void sortAndWrite()  {
        Planes planes = gson.fromGson();
        gson.toGson(planes);
    }

    public void readSortedGson() {
        Planes result = gson.fromGsonSorted();
        System.out.println(result);
    }

    public void quit() {
        log.info("user quit");
        log.trace("application endpoint");
        System.exit(0);
    }
}
