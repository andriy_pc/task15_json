package app.viewer;

import java.util.ArrayList;

public class Viewer {
    private ArrayList<String> menu;

    public Viewer() {
        init();
    }

    private void init() {
        menu = new ArrayList<>();
        menu.add("0 - quit");
        menu.add("1 - validate .json with schema");
        menu.add("2 - read json with gson");
        menu.add("3 - sort and write to sorted.json");
        menu.add("4 - read sorted .json");
    }

    public void display() {
        menu.stream()
                .forEach(s -> System.out.println(s));
    }
}
