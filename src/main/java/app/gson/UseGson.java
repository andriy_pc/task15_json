package app.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.stream.JsonWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import units.*;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class UseGson {

    private Gson gson;
    private Logger log = LogManager.getLogger();

    public Planes fromGson() {
        gson = new Gson();
        Planes planes = null;
        try (FileReader fr = new FileReader("files\\planes.json")) {
            planes = gson.fromJson(fr, Planes.class);
        } catch (FileNotFoundException e) {
            log.error("Wrong path to file!");
        } catch (IOException e) {
            log.error("IOException: ");
            e.printStackTrace();
        }
        return planes;
    }

    public void toGson(Planes planes) {
        ArrayList<Plane> list = planes.getPlanes();
        list.sort((p1,  p2) -> {
            return p1.getModel().compareTo(p2.getModel());
        });
        planes.setPlanes(list);

        GsonBuilder gBuilder = new GsonBuilder();
        gBuilder.setPrettyPrinting();
        Gson gCustom = gBuilder.create();

        JsonElement element = gson.toJsonTree(planes);

        try(FileWriter out =
                    new FileWriter("files\\sortedPlanes.json");
            JsonWriter jWriter = gCustom.newJsonWriter(out)) {
            gson.toJson(element, jWriter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Planes fromGsonSorted() {
        Gson gson = new Gson();
        Planes result = null;
        try (FileReader sortedJson = new FileReader("files\\sortedPlanes.json")) {
            result = gson.fromJson(sortedJson, Planes.class);
        } catch (FileNotFoundException e) {
            log.error("wrong file path");
            e.printStackTrace();
        } catch (IOException e) {
            log.error("IOException");
            e.printStackTrace();
        }
        return result;
    }
}
