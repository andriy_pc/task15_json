package app;

import app.viewer.Viewer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger log = LogManager.getLogger();
    public static void main(String[] args) {
        log.info("application entrance");
        Controller c = new Controller(new Viewer(), new Model());
        while(true) {
            c.run();
        }
    }
}
