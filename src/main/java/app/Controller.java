package app;
import java.util.InputMismatchException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import app.viewer.Display;
import app.viewer.Viewer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
    Scanner in = new Scanner(System.in);
    private Logger log = LogManager.getLogger();
    private Viewer viewer;
    private Model model;
    private Map<Integer, Display> functions;

    private Display display;
    private int choice;

    public Controller(Viewer v, Model m) {
        viewer = v;
        model = m;
        init();
    }

    private void init() {
        functions = new LinkedHashMap<>();
        functions.put(0, model::quit);
        functions.put(1, model::validate);
        functions.put(2, model::readGson);
        functions.put(3, model::sortAndWrite);
        functions.put(4, model::readSortedGson);
    }

    public void run() {
        viewer.display();
        try {
            choice = in.nextInt();
            display = functions.get(choice);
        } catch (InputMismatchException e) {
            log.error("wrong user's input!");
            e.printStackTrace();
        }
        if(functions.containsKey(choice)) {
            display.display();
        } else {
            log.fatal("user put number, than is absent in functions!");
            throw new RuntimeException("Out of keys!");
        }

    }
}