package validation;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;

public class Validation {
    private static Logger log = LogManager.getLogger();
    private JSONObject jsonToValidate;

    public boolean validate(String pathToSchema, String pathToJSON) {
        try (
                InputStream schemaInput = new FileInputStream(pathToSchema);
                InputStream jsonInput = new FileInputStream(pathToJSON)) {

            JSONObject rawSchema = new JSONObject(new JSONTokener(schemaInput));
            //specifying draft version
            SchemaLoader schemaLoader = SchemaLoader.builder()
                    .schemaJson(rawSchema)
                    .draftV7Support()
                    .build();
            //building schema
            Schema schema = schemaLoader.load().build();
            try {
                //ValidationException is thrown if schema is invalid
                schema.validate(new JSONObject(new JSONTokener(jsonInput)));
            } catch (ValidationException ve) {
                log.error("validation exception!");
                log.error("Schema is invalid!");
                return false;
            }
        } catch (IOException e) {
            log.error("validate: IOException");
            e.printStackTrace();
        }
        return true;
    }
}
